require 'RMagick'
require 'net/ssh'
require 'net/sftp'

class Image

  attr_accessor :name, :crop_offset_x, :crop_offset_y, :crop_width, :crop_height, :url, :resize_width, :resize_height, :type

  def initialize(attr)
    @name = attr[:name] || attr['name']
    @crop_offset_x = attr[:crop_offset_x] || attr['crop_offset_x']
    @crop_offset_y = attr[:crop_offset_y] || attr['crop_offset_y']
    @crop_width = attr[:crop_width] || attr['crop_width']
    @crop_height = attr[:crop_height] || attr['crop_height']
    @resize_width = attr[:resize_width] || attr['resize_width']
    @resize_height = attr[:resize_height] || attr['resize_height']
    @type = attr[:type] || attr['type']
    @url = attr[:type] || attr['type']
  end

  def crop_and_upload_image
    return false if !File.exist?(IMAGE_TEMPORARY_DIRECTORY + name) || crop_offset_x.blank? || crop_offset_y.blank? || crop_width.blank? || crop_height.blank?

    timestamp = Time.now.to_i.to_s
    original_image = Magick::Image::read(IMAGE_TEMPORARY_DIRECTORY + name).first

    unless original_image.columns.to_i == resize_width.to_i && original_image.rows.to_i == resize_height.to_i
      crop_image = original_image.crop(crop_offset_x.to_i, crop_offset_y.to_i, crop_width.to_i, crop_height.to_i, true)
      crop_image.write(IMAGE_TEMPORARY_DIRECTORY + 'cropped_' + name)

      resized_image = crop_image.resize(resize_width.to_i, resize_height.to_i)
      resized_image.write(IMAGE_TEMPORARY_DIRECTORY + 'resize_' + name)
      type.eql?("team") ? upload_files(['resize_' + name]) : upload_files(['resize_' + name, name])
    else
      upload_files([name])
    end
  end

  def crop_and_upload_featured_images
    return false if !File.exist?(IMAGE_TEMPORARY_DIRECTORY + name) || crop_offset_x.blank? || crop_offset_y.blank? || crop_width.blank? || crop_height.blank?

    timestamp = Time.now.to_i.to_s
    original_image = Magick::Image::read(IMAGE_TEMPORARY_DIRECTORY + name).first

    crop_image = original_image.crop(crop_offset_x.to_i, crop_offset_y.to_i, crop_width.to_i, crop_height.to_i, true)
    crop_image.write(IMAGE_TEMPORARY_DIRECTORY + 'cropped_' + name)

    thumbnail = crop_image.resize(264, 148)
    thumbnail.write(IMAGE_TEMPORARY_DIRECTORY + 'thumbnail_' + name)

    hero = crop_image.resize(624, 351)
    hero.write(IMAGE_TEMPORARY_DIRECTORY + 'hero_' + name)

    upload_files([ name, "hero_#{name}", "thumbnail_#{name}" ])
  end

  def upload_files(images)
    timestamp = Time.now.to_i.to_s
    new_remote_dir = type.eql?("team") ? CDN_TEAM_PHOTO_DIR + timestamp : CDN_STUDIO_TEMPLATE_DIR + timestamp
    urls = []

    Net::SSH.start(
      CDN_SERVER, CDN_USER ,
      :host_key => "ssh-rsa",
      :keys => [CDN_SSH_KEY_PATH]
    ) do |ssh|
      ssh.sftp.connect do |sftp|
        begin
          sftp.mkdir!(new_remote_dir, :permissions => 0755)

          images.each do |image_name|
            cdn_image_url = type.eql?("team") ? "#{CDN_TEAM_PHOTO_PREFIX_URL}#{timestamp}/#{image_name}" : "#{STUDIO_TEMPLATE_CDN_PREFIX_URL}#{timestamp}/#{image_name}"

            sftp.upload!(IMAGE_TEMPORARY_DIRECTORY + image_name, new_remote_dir + "/" + image_name)

            urls << cdn_image_url
          end

          return urls
        rescue Exception => e
          return false
        end
      end
    end
  end

  # Upload a file into CDN
  def self.upload_file(file_name)
    timestamp = Time.now.to_i.to_s
    new_remote_dir = CDN_STUDIO_TEMPLATE_DIR + timestamp

    Net::SSH.start(
      CDN_SERVER, CDN_USER ,
      :host_key => "ssh-rsa",
      :encryption => "blowfish-cbc",
      :keys => [CDN_SSH_KEY_PATH]
    ) do |ssh|
      ssh.sftp.connect do |sftp|
        begin
          sftp.mkdir!(new_remote_dir, :permissions => 0755)
          cdn_file_url = "#{STUDIO_TEMPLATE_CDN_PREFIX_URL}#{timestamp}/#{file_name}"
          sftp.upload!(IMAGE_TEMPORARY_DIRECTORY + file_name, new_remote_dir + "/" + file_name)
          return cdn_file_url
        rescue Exception => e
          return false
        end
      end
    end
  end

end
